import React, {Component} from 'react';
import axios from 'axios';
import config from '../config.js'
import ScreenMQ from './ScreenMQ';
import ScreenCQ from './ScreenCQ';
import ScreenRQ from './ScreenRQ';
import ResponseManage from './ResponseManage';

class MessageManage extends Component{
  constructor(){
    super();
    this.state = {screen:0}
  }

  componentDidMount(){
    this.serviceCaller();
  }

  serviceCaller = ()=>{
    let token = window.sessionStorage.getItem("token")
    var headers = {
       headers : {
            'Content-Type': 'application/x-www-form-urlencoded',
            "username": "admin",
            'token': token
        }
      }
      axios.get(`${config.rootUrl}/users`,headers)
            .then(function (response) {
              if(!response.data.success){
                this.setState({userList: response.data})
              }
              else{
                  this.setState({err : response.data.success})
              }
            }.bind(this))
            .catch(function (error) {
                // this.setState({err : response.data.status})
              console.log(error);
            });
  }


  sidebar= ()=>{
    return(
        <div className= "sideBarHead">
          <ul className="sideBarUl" onClick = {(event)=>{this.setState({screen:event.target.id })}} >
            <li className={`sideBarli ${this.state.screen==0?"selected":""}`}  id = {0}> Create Question </li>
            <li className={`sideBarli ${this.state.screen==1?"selected":""}`} id = {1}> Manage Questions </li>
            <li className={`sideBarli ${this.state.screen==2?"selected":""}`} id = {2} > Patient Respones </li>
          </ul>
        </div>
    )
  }

  mainScreen=(screen)=>{
    return <div> {screen} </div>
  }

  render(){
    return(
        <div style={{    display: "-webkit-box", paddingTop:50}}>
          {this.sidebar()}
          {(this.state.userList)?(this.state.screen == 0)?<ScreenCQ users= {this.state.userList} token={this.props.token}/>:(this.state.screen == 1)?<ScreenMQ token={this.props.token} users= {this.state.userList}/>:<ScreenRQ token={this.props.token} users= {this.state.userList}/>:"user list loading"}
        </div>
    )
  }

}

export default MessageManage
