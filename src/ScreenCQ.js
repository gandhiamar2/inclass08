import React, {Component} from 'react';
import axios from 'axios';
import config from '../config.js'
import 'rc-time-picker/assets/index.css';
import moment from 'moment';
import TimePicker from 'rc-time-picker';
import qs from 'qs';


const showSecond = true;
const str = showSecond ? 'HH:mm:ss' : 'HH:mm';

class ScreenCQ extends Component{
  timeArray = [moment(),moment(),moment()]
  constructor(){
    super();
    this.state = {checked:true,checked2:[false,false,false],value:""}
  }

  addQ= ()=>{
    if(this.state.value){
      let data = {}
       data["message"] = this.state.value
       data["isquestion"] = this.state.checked
       data["isreminder"] = (this.state.checked2.findIndex(el => el == true))>=0?true:false
       data["remindtype"] = (this.state.checked2.findIndex(el => el == true))
       let temptimeArray = []
       this.timeArray.map((el,index)=>{
         temptimeArray[index] = el.format("HH:mm:ss");
       })
       temptimeArray.push(moment().format("HH:mm:ss"))
       switch (this.state.checked2.findIndex(el => el == true)) {
         case 0:
            data["remind"] = temptimeArray.slice(0,1)
           break;
        case 1:
            data["remind"] = temptimeArray.slice(3)
          break;
        case 2:
            data["remind"] = temptimeArray.slice(1,3)
            break;
         default:

       }

      let token = window.sessionStorage.getItem("token");
      var headers = {
         headers : {
              'Content-Type':'application/json',
              "username": "admin",
              'token': token
          }
        }
      axios.post(`${config.rootUrl}/message`,data,headers)
            .then(function (response) {
              if(response.data.success){
                this.setState({value: ""})
              }
              else{
                  this.setState({err : response.data.error})
              }
            }.bind(this))
            .catch(function (error) {
              // if(!response.data.success)
              //   this.setState({err : response.data.status})
              console.log(error);
            });
    }
  }

  checkbox2Setter=(id)=>{
    let temp = [false,false,false]
    if(!this.state.checked2[id]){
      temp[id] = true;
    }
    this.setState({checked2: temp})
  }

  componentWillUnMount(){

  }
  onChangeTime=(id,m)=>{
    this.timeArray[id] = m
  }

  timePicker = ()=>{
    if(this.state.checked2[0]){
        return <div style={{marginLeft:"67%", marginTop:40}}><TimePicker style={{ width: 70 }} showSecond={showSecond} defaultValue={moment()} className="xxx" onChange={(m)=>this.onChangeTime(0,m)}/> </div>
    }
    else if(this.state.checked2[2]){
      return  <div style={{marginLeft:"64%", marginTop:40}}>
                <TimePicker style={{ width: 70 }} showSecond={showSecond} defaultValue={moment()} className="xxx" onChange={(m)=>this.onChangeTime(1,m)}/>
                <TimePicker style={{ width: 70 }} showSecond={showSecond} defaultValue={moment()} className="xxx" onChange={(m)=>this.onChangeTime(2,m)}/>
              </div>
    }
  }

  render(){
    return(
      <div style={{marginLeft: "13%"}}>
        <div><input className= "cqInput" placeholder="Please Enter your Question here...." value= {this.state.value} onChange={event=>this.setState({value: event.target.value})}/></div>
        <div className= "cqCheck"><input className="cqInputCheck" checked={this.state.checked} onChange={event=>this.setState(prevState=>({checked: !prevState.checked}))}  type="checkbox"/><div> Make it a Yes or No Question </div></div>
        <div style={{    marginLeft: "45%"}}className= "cqCheck">
          <input className="cqInputCheck"  id={0} checked={this.state.checked2[0]} onChange={(e)=>this.checkbox2Setter(e.target.id)}  type="checkbox"/><div style={{marginRight :100}}> Daily Once </div>
          <input className="cqInputCheck" id={1} checked={this.state.checked2[1]} onChange={(e)=>this.checkbox2Setter(e.target.id)}  type="checkbox"/><div style={{marginRight :100}}> Every Hour </div>
          <input className="cqInputCheck" id={2} checked={this.state.checked2[2]} onChange={(e)=>this.checkbox2Setter(e.target.id)}  type="checkbox"/><div style={{marginRight :100}}> Daily Twice </div>
        </div>
        {this.timePicker()}
        <div ><button className = "cqButton" onClick= {()=>this.addQ()}> {(this.state.checked)?'Add Question': 'Add Message'} </button></div>
      </div>
    )
  }
}



export default ScreenCQ;
