import React, {Component} from 'react';
import axios from 'axios';
import config from '../config.js'
import qs from 'qs';
import { Menu, Dropdown, Icon, message } from 'antd';
import style from '../styles/dropdownstyle.css'
import ResponseAnalyse from './ResponseAnalyse';


class ScreenRQ extends Component{
  constructor(){
    super();
    this.state={messages:undefined,selectedMenuUser: undefined}
  }

  componentDidMount(){
    this.serviceCaller()
  }

  serviceCaller = ()=>{
    let token = window.sessionStorage.getItem("token")

    var headers = {
       headers : {
            'Content-Type': 'application/x-www-form-urlencoded',
            "username": "admin",
            'token': token
        }
      }

    axios.get(`${config.rootUrl}/responses`,headers)
          .then(function (response) {
            if(response.data.success){
              this.setState({survey: response.data.responses})
            }
            else{
                this.setState({err : response.data.status})
            }
          }.bind(this))
          .catch(function (error) {
              // this.setState({err : response.data.status})
            console.log(error);
          });
  }

  getInfoByUser = (username)=>{
    let token = window.sessionStorage.getItem("token");
    var headers = {
       headers : {
            'Content-Type':'application/json',
            'token': token
        }
      }
    axios.get(`${config.rootUrl}/getmessage?username=${username}`,headers)
          .then(function (response) {
            if(response.data.success){
              let surveyres;
              this.state.survey.map(sur=>{
                 if(sur.username == username)
                    surveyres = sur
              })
              if(surveyres){
                surveyres = surveyres.responses
              }
              this.setState({responses: response.data.responses,messages: response.data.message,selectedMenuUser: username, surveyres: surveyres})
            }
            else{
                this.setState({err : response.data.error})
            }
          }.bind(this))
          .catch(function (error) {
            console.log(error);
          });
  }

  menuOnClick =  ({key})=> {
    // this.setState({selectedMenuUser: key})
    this.getInfoByUser(this.props.users[key].username)
    // message.info(`Click on item ${key}`);
  };

  assignMenu = ()=> {

    const menuItemGetter = ()=>{
      return this.props.users.map((user,id)=>{
          return <Menu.Item key={id}>{user.username}</Menu.Item>
      })
    }
    return   <Menu onClick={this.menuOnClick}>
                {menuItemGetter()}
             </Menu>
  }

  dropDownMenu = ()=>{

    return  <Dropdown overlay={this.assignMenu()}>
                <div className="dropdown" style={{ marginTop: 20,marginLeft: 550,color: "antiquewhite"}}>
                  {(this.state.selectedMenuUser)?this.state.selectedMenuUser:"select user"}
                 </div>
            </Dropdown>
  }

  responseList=()=>{
    let user = this.props.users[this.state.selectedMenuUser];
    let qResp = {};
    let returnArray = []
    this.state.messages.map(msg=>{
        qResp[msg.mid] = {}
        qResp[msg.mid]["isquestion"] = msg.isquestion;
        qResp[msg.mid]["isresponded"] = false
        qResp[msg.mid]["message"] = msg.message
        qResp[msg.mid]["response"] = []
    })
    this.state.responses.map(resp=>{
      if(qResp[resp.qid]){
        qResp[resp.qid]["isresponded"] = true
        qResp[resp.qid]["response"] = resp.responses
      }
      else{
        console.log("bug at line 79")
      }
    })
    returnArray.push(<li key="0">
        <div style={{display:"flex",width:1200}}>
          <div key="1"style={{   fontWeight: "bolder", flex: 100}}>Messsage Content</div>
          <div key="2"style={{   fontWeight: "bolder", flex: 40}}>Question</div>
          <div key="3"style={{   fontWeight: "bolder", flex: 40}}>Responded</div>
          <div key="4"style={{  fontWeight: "bolder",  flex: 40}}>Response</div>
        </div>
     </li>);

    returnArray.push(Object.keys(qResp).map(qresp=>{
      return <li key={qresp}>
              <div style={{display:"flex",width:1200}}>
                <div key="1" style={{    flex: 100}}>{qResp[qresp].message}</div>
                <div key="2"style={{    flex: 40}}>{qResp[qresp]["isquestion"]+""}</div>
                <div key="3"style={{    flex: 40}}>{qResp[qresp].isresponded+""}</div>
                <div key="4"style={{    flex: 40}}>{(qResp[qresp].response[0])?qResp[qresp].response[0]:"no response"}</div>
              </div>
             </li>
    }));

    return returnArray;

  }

  printPage=()=>{
    window.print();
  }

  render(){
    return(
      <div style={{marginLeft: "13%"}}>
        <div key="0">{this.dropDownMenu()} <button className="rqButton"onClick={this.printPage}>Print</button></div>
        {(this.state.selectedMenuUser)? <div>
          <div style={{marginLeft:125,marginTop:50}}><ul>{this.responseList()}</ul></div>
            {(this.state.surveyres)?<div><ResponseAnalyse response={this.state.surveyres}/></div>:""}</div>
            :<div style={{marginTop:50, marginLeft:"75%"}}>Select User</div>}
      </div>
    );
  }
}


export default ScreenRQ;
