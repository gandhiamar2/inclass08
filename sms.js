var twilio = require('twilio');

var accountSid = 'ACdb6c976790043659dc077118a4efc5fb'; // Your Account SID from www.twilio.com/console
var authToken = '89cd333ae38b2a4b4368acf28f3e4d9b';   // Your Auth Token from www.twilio.com/console

var client = new twilio(accountSid, authToken);
var userInteraction = {}

const sendTestSms = (to,body)=>{
  client.messages.create({
      body: body,
      to: to,  // Text this number
      from: '+18038100265' // From a valid Twilio number
  })
  .then((message) => console.log(message.sid));
}

const question1 = ()=>{
  const qFeedback=()=>{
    return "Thank you and we will check with you later."
  }

  let q1 = {
    type: 1,
    que: "Please indicate your symptom (1)Headache, (2)Dizziness, (3)Nausea, (4)Fatigue, (5)Sadness, (0)None",
    isSent: false,
    isResponded: false,
    response: undefined,
    range: [1,1,2,3,4,5],
    wrongEntry: "Please enter a number from 0 to 5",
    feedback: qFeedback,
    feedSent: false,
    symptomArray: ["None","Headache","Dizziness","Nausea","Fatigue","Sadness"],
    symptom: undefined,
    actionDone: false
  }
  return q1
}


const question2 = (symptom)=>{

   const qFeedback=(q2)=>{
     return `You have a ${q2.symptomArray[q2.response]} ${symptom}`
   }

  let q2 = {
    type:2,
    que: `On a scale from 0 (none) to 4 (severe), how would you rate your ${symptom} in the last 24 hours?`,
    isSent: false,
    isResponded: false,
    response: undefined,
    range: [1,1,2,3,4],
    wrongEntry: "Please enter a number from 0 to 4",
    feedback: qFeedback,
    feedbackSent: false,
    symptomArray: ["None","mild","mild","moderate","severe"],
    symptom: undefined,
    actionDone: false
  }
  return q2
}

const responseProcessing=(req,res)=>{
  let msgFrom = req.body.From;
  let msgBody = req.body.Body;
  let questionsArray = userInteraction[msgFrom];
  let questionPosition;
  let nextQuestion;
  let workingQuestion = questionsArray[questionsArray.length-1]
  if(!workingQuestion.isSent){
            workingQuestion.isSent = true
        res.send(`<Response>
                    <Message>
                      welcome to the study
                    </Message>
                  </Response>`)
        sendTestSms(msgFrom, workingQuestion.que)
  }else if(!workingQuestion.actionDone) {
    if(workingQuestion.range[msgBody]){
        workingQuestion.isResponded = true;
        workingQuestion.response = msgBody;
        workingQuestion.symptom = workingQuestion.symptomArray[msgBody];
        workingQuestion.feedbackSent = true;
        workingQuestion.actionDone = true;  // can be moved scucees sendTestSent
        if(workingQuestion.type==1){
          nextQuestion = question2(workingQuestion.symptom);
          nextQuestion.isSent = true;

          if(msgBody==0)
            res.send(`<Response>
                      <Message>
                        ${workingQuestion.feedback()}
                      </Message>
                    </Response>`)
          else{
            questionsArray.push(nextQuestion)
            res.send(`<Response>
                        <Message>
                          ${nextQuestion.que}
                        </Message>
                      </Response>`)}
        }else{
          nextQuestion = question1();
          nextQuestion.isSent = true;
            if(questionsArray.length<6){
              questionsArray.push(nextQuestion)
              res.send(`<Response>
                          <Message>
                            ${workingQuestion.feedback(workingQuestion)}
                          </Message>
                        </Response>`)
                      sendTestSms(msgFrom, nextQuestion.que)}
            else{
              res.send(`<Response>
                          <Message>
                            ${workingQuestion.feedback(workingQuestion)}
                          </Message>
                        </Response>`)
                      sendTestSms(msgFrom, "Thank you and see you soon")}
        }

    }else{
      res.send(`<Response>
                  <Message>
                    ${workingQuestion.wrongEntry}
                  </Message>
                </Response>`)
    }
  }else{
    res.send(`<Response>
                <Message>
                  survey done
                </Message>
              </Response>`)
  }
}

const newsms = (req,res)=>{
    console.log(req.body)
    let msgFrom = req.body.From;
    let msgBody = req.body.Body;
    if(msgBody=="start" || msgBody=="START"){
      if(!userInteraction[msgFrom]){
          let questionsArray = [question1()]
          userInteraction[msgFrom] = questionsArray
          console.log("started")
          responseProcessing(req,res)
      }
      else{
        res.send(`<Response>
                    <Message>
                      Hello ${msgFrom} you have already registered with us
                    </Message>
                  </Response>`)
      }
    }else if(userInteraction[msgFrom]){
        responseProcessing(req,res)
    }else{
      res.send(`<Response>
                  <Message>
                    Hello ${msgFrom} you haven't registered with us send start to register
                  </Message>
                </Response>`)
    }
}



export default {sendTestSms,newsms}
