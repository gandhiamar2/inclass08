import mongoose from 'mongoose';

const user = new mongoose.Schema({
    'id': {
        'type': Number,
        'required': true,
        'unique': true
    },
    'first_name': {
        'type': String,
        'required': true
    },
    'last_name':{
        'type': String,
        'required':true
    },
    'email': {
        'type': String,
        'required': true
    },
    'gender':{
        'type': String,
        'required': true
    },'ip_address':{
      'type': String,
      'required': true
    }

});

export default mongoose.model('Usersinclass08', user);
