package advinclass08.example.com.inclassadv08;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Rama Vamshi Krishna on 10/19/2016.
 */
public class MyAdapter extends  RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<User> list_of_users;
    private Context mContext;
    static MainActivity activity;
    public static final int USER_PRESENT = 0;
    public static final int NO_USER = 1;
    public MyAdapter(Context context, List<User> itemslist, MainActivity activity) {
        list_of_users = itemslist;
        mContext = context;
        this.activity=activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == USER_PRESENT) {
            View imsg_row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, null);
            return new UserViewHolder(imsg_row);
        } else {
            View sur_msg_row = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_loadmore, null);
            return new LoadMoreViewHolder(sur_msg_row);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final int itemType = getItemViewType(position);
        if (itemType == USER_PRESENT) {
            User user = list_of_users.get(position);
            TextView txtname = ((UserViewHolder) holder).txtfname;
            TextView txtemail = ((UserViewHolder) holder).txtemail;
            TextView txtlname = ((UserViewHolder) holder).txtlname;
            TextView txtgender = ((UserViewHolder) holder).txtgender;
            TextView txtip = ((UserViewHolder) holder).txtip;
            txtname.setText(user.getFirstname().toString());
            txtlname.setText(user.getLastname().toString());
            txtgender.setText(user.getGender().toString());
            txtip.setText(user.getIpaddress().toString());
            txtemail.setText(user.getEmail().toString()+", ");

        } else {
            Button load_more = ((LoadMoreViewHolder) holder).load_more;

        }
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        public TextView txtfname, txtlname, txtemail,txtgender,txtip ;
        public UserViewHolder(View itemView) {
            super(itemView);
            txtfname = (TextView) itemView.findViewById(R.id.tv1);
            txtlname = (TextView) itemView.findViewById(R.id.tv2);
            txtemail = (TextView) itemView.findViewById(R.id.tv3);
            txtgender = (TextView) itemView.findViewById(R.id.tv4);
            txtip = (TextView) itemView.findViewById(R.id.tv5);
        }

    }

    public static class LoadMoreViewHolder extends RecyclerView.ViewHolder {
        public Button load_more;
        MaintainData2 mdata2;
        public LoadMoreViewHolder(View itemView) {
            super(itemView);
            load_more = (Button) itemView.findViewById(R.id.btnload_more);
            load_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.fetchAdditionalRecords(getAdapterPosition());
                }
            });
        }

        public interface MaintainData2{
            public void fetchAdditionalRecords(int position);
        }


    }



    @Override
    public int getItemCount() {
        return list_of_users.size();
    }


    @Override
    public int getItemViewType(int position) {
        if (list_of_users.get(position).userid!=null) {
            return USER_PRESENT;
        }
        else {
            return NO_USER ;
        }
    }



}

