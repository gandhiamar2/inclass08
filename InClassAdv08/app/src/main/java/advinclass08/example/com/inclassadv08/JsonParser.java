package advinclass08.example.com.inclassadv08;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Rama Vamshi Krishna on 08/30/2017.
 */
public class JsonParser {




    static class ParseUsersList {
        static ArrayList<User> doJsonParsing(String inputstring) throws JSONException {
            ArrayList<User> questions_list = new ArrayList<User>();
            JSONObject rootobj = new JSONObject(inputstring);
            JSONArray qarray = rootobj.getJSONArray("userList");
            if (qarray!=null && qarray.length() > 0) {
                for (int i = 0; i < qarray.length(); i++) {
                    User qobj = new User();
                    JSONObject queobj = qarray.getJSONObject(i);
                    qobj.setUserid(queobj.optString("id"));
                    qobj.setFirstname(queobj.optString("first_name"));
                    qobj.setLastname(queobj.optString("last_name"));
                    qobj.setEmail(queobj.optString("email"));
                    qobj.setGender(queobj.optString("gender"));
                    qobj.setIpaddress(queobj.optString("ip_address"));
                    questions_list.add(qobj);
                }
                return questions_list;
            }
            return null;
        }
    }

}

