package advinclass08.example.com.inclassadv08;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements MyAdapter.LoadMoreViewHolder.MaintainData2 {

    Spinner property_spinner, order_spinner;
    private String[] propertySpinner,orderSpinner;
    List<User> list_of_users = new ArrayList<>();
    ArrayList<User> tmp_list = new ArrayList<>();
    RecyclerView users_view;
   int current_page = 0;
   String current_format = "id";
   int order = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSpinners();
        setRecycleView();
        property_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                current_page = 0;
                switch (i){
                    case 0:
                        current_format = "id";
                        break;
                    case 1:
                        current_format = "first_name";
                        break;
                    case 2:
                        current_format = "last_name";
                        break;
                    case 3:
                        current_format = "gender";
                        break;
                    default:
                        break;
                }
                list_of_users = new ArrayList<User>();
                setRecycleView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        order_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                current_page = 0;
                switch (i){
                    case 0:
                        order = 1;
                        break;
                    case 1:
                        order = 1;
                        break;
                    case 2:
                        order = -1;
                        break;
                    default:
                        break;
                }
                list_of_users = new ArrayList<User>();
                setRecycleView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setRecycleView() {
        if(current_page<=19) {
            getUsersListFromApi();
        }
        else{
            displayToast("User list is complete...");
        }
    }

    private void getUsersListFromApi() {
            tmp_list=new ArrayList<User>();
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                .url(getURLToRetrieveFiftyMessages())
                .get()
                .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        Log.d("Hll","Failed");
                    } else {
                        try {
                            tmp_list = JsonParser.ParseUsersList.doJsonParsing(response.body().string());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (tmp_list != null && tmp_list.size() > 0) {
                                    list_of_users.addAll(tmp_list);
                                    setAdapterAndLayoutsForRecycleView();
                                } else {
                                    displayToast("Empty List. Try again");
                                }
                            }
                        });
                    }
                }
            });

    }

    private void displayToast(String s) {
        Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG).show();
    }

    private void setAdapterAndLayoutsForRecycleView() {
        if(list_of_users.size()>100){
            List<User> newlist =  new ArrayList<>(list_of_users.subList(50,150));
            list_of_users = new ArrayList<>(newlist);
        }
        ArrayList<User> dy = new ArrayList<User>();
        dy.addAll(list_of_users);
        dy.add(new User());
        MyAdapter useradapter = new MyAdapter(this,  dy, MainActivity.this);
        users_view.setAdapter(useradapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        layoutManager.setSmoothScrollbarEnabled(true);
        users_view.setLayoutManager(layoutManager);
        if(list_of_users.size()>50){
            users_view.scrollToPosition(49);
        }
    }

    private String getURLToRetrieveFiftyMessages() {
        return getResources().getString(R.string.API_URL)+current_page+"?format="+current_format+"&order="+order;
    }

    private void setSpinners() {
        users_view = (RecyclerView)  findViewById(R.id.user_view);
        propertySpinner = new String[] {
                "SelectOne","First Name", "Last Name", "Gender"
        };
        orderSpinner = new String[] {
                "SelectOne","Ascending", "Descending"
        };
        order_spinner = (Spinner) findViewById(R.id.spinner_order);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, orderSpinner);
        order_spinner.setAdapter(adapter);
        property_spinner = (Spinner) findViewById(R.id.spinner_property);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, propertySpinner);
        property_spinner.setAdapter(adapter1);

    }


    @Override
    public void fetchAdditionalRecords(int position) {
        current_page++;
        setRecycleView();
    }
}
