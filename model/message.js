import mongoose from 'mongoose';


const message = new mongoose.Schema({
    'message':{
        'type': String,
        'required':true
    },
    'isquestion': {
        'type': Boolean,
        'required': true
    },
    'users':{
        'type':[String],
        'required':false
    },
    'isreminder':{
        'type': Boolean,
        'required': true
    },
    'remind':{
      'type': [String],
      'required':false
    },
    'remindtype':{
      'type': Number,
      'required':false
    },'ispublished':{
      'type': Boolean,
      'required': true,
      'default': false
    },'publishedtime':{
      'type': String,
      'required': false
    },'mid':{
        'type': String,
        'required':true,
        'unique':true
    }
});

export default mongoose.model('Message6', message);
