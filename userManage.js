// import data from './resources/USER_MOCK_DATA.csv';
var fs = require('fs');
import path from 'path';
var exec = require('child_process').exec;
var execsync = require("child_process").execSync;

import User from './models/user';


const getThem =(req,res)=>{


    let returnValue = (err,data)=>{
        if(err)
        {
            // res.status(200).json({success:false,status:'all User data failed'});
            res.send({limitExceeded:true,success:false})
        } else
        {
          // console.log(data)
          res.send({limitExceeded:false,success:true,userList:data})
        }
    }

  let page = req.params.pageno;
  let filter = req.query.format;
  let order = req.query.order;
  let limit = 50;
  let maxLimit = 1000;
  let skipLimit = 50*page;
  if(page>19){
    res.send({limitExceeded:true,success:false})
  }else if(page<0){
    res.send({limitExceeded:true,success:false})
  }else{
    console.log("in here")
    let sort = {}
    sort[filter] = order;
      // let userList = User.find({}, {sort:sort, limit:50,skip:skipLimit},returnValue);
      User.find({}).sort(sort).limit(50).skip(skipLimit).exec(returnValue)
  }

}

const setThem = ()=>{
  let userList = [];
  var data = fs.readFileSync(path.join(__dirname,'./resources/USER_MOCK_DATA.csv'),'utf8');
  let splittedData = data.split('\n')
  splittedData.unshift();
  splittedData.map((entry,pos)=>{
    if(pos!=0){
    let cats = entry.split(',');
    let user={
        'id':cats[0],
        'first_name': cats[1],
        'last_name': cats[2],
        'email': cats[3],
        'gender': cats[4],
        'ip_address': cats[5],
    };
    userList.push(user);
  }
  })
  userList.pop()
  try {
       let result = User.insertMany(userList);
    console.log("we are done")
    console.log(result)
    } catch (e) {
       print (e);
    }
}

export default {getThem,setThem};
