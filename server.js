var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');
var express =  require('express');
var app = express();
import bodyParser from 'body-parser';
import tweetservice from './services/tweetservice'
import webServices from './services/webservices'
import mongoose from 'mongoose';
import confi from './config.js';
import expressJWT from 'express-jwt';
import jwt from 'jsonwebtoken';
// import router from './routes';
import sms from './sms'
import users from './userManage'

mongoose.connect(confi.db.uri, {}, (err)=> {
  if (err) {
    console.log('Connection Error: ', err);
  } else {
    console.log('Successfully Connected');
  }
});

app.use(bodyParser.urlencoded({extended: false}));

app.post('/users',users.setThem)

app.get('/users/:pageno',users.getThem)


app.listen(3000, '0.0.0.0', function (err, result) {
    if (err) {
      console.log(err);
    }
    console.log('Running at http://0.0.0.0:3000');
  });
